import { Inject } from '@nestjs/common';
import { ParkSlotRepository } from '../interface/ParkSlotRepository.interface';

export class DeleteParkSlot {
  public constructor(
    @Inject(ParkSlotRepository)
    private parkSlotRepository: ParkSlotRepository,
  ) {}

  public async handle(id: string): Promise<void> {
    this.parkSlotRepository.delete(id);
  }
}
