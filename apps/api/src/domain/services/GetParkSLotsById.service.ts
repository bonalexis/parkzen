import { Inject } from '@nestjs/common';
import { ParkSlotRepository } from 'src/domain/interface/ParkSlotRepository.interface';
import { ParkSlot } from '../entities/ParkSlot';

export class GetParkslotsById {
  public constructor(
    @Inject(ParkSlotRepository)
    private parkSlotRepository: ParkSlotRepository,
  ) {}

  public async handle(id: string): Promise<ParkSlot[]> {
    const listParkSlotById = await this.parkSlotRepository.getParkSlotById(id);
    return listParkSlotById;
  }
}
