import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import { CreateParkDTO } from '../../dto/CreatePark.dto';
import { CreateParkSlot } from '../../domain/services/CreateParkSlot.service';
import { ListParkSlot } from 'src/domain/services/ListParkSlot.service';
import { DeleteParkSlot } from 'src/domain/services/DeleteParkSlot.service';
import { GetParkslotsById } from 'src/domain/services/GetParkSLotsById.service';

@Controller('parks')
export class ParkSlotController {
  constructor(
    private readonly createParkSlot: CreateParkSlot,
    private readonly listParkSlots: ListParkSlot,
    private readonly deleteParkSlot: DeleteParkSlot,
    private readonly getParkSlotsById: GetParkslotsById,
  ) {}

  @Post()
  createPark(@Body() body: CreateParkDTO) {
    this.createParkSlot.handle(body);
    return HttpStatus.NO_CONTENT;
  }

  @Get()
  async listPark() {
    const parkSlots = await this.listParkSlots.handle();
    return parkSlots;
  }

  @Get(':id')
  async getParkSlotsProprietaire(@Param('id') id: string) {
    const parkSlots = await this.getParkSlotsById.handle(id);
    return parkSlots;
  }

  @Delete(':id')
  async deletePark(@Param('id') id: string) {
    this.deleteParkSlot.handle(id);
    return HttpStatus.OK;
  }
}
