const apiHost = process.env.REACT_APP_API_HOST;

async function getParkSlotById(id: string) {
  const requestPath = "/parks";
  const url = new URL(`${apiHost}${requestPath}/${id}`);

  const requestInit: RequestInit = {
    method: "GET",
  };

  return fetch(url, requestInit).then((res) => res.json());
}

export default getParkSlotById;
