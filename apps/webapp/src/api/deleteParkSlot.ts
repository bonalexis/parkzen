const apiHost = process.env.REACT_APP_API_HOST;

async function deleteParkSlot(id: string) {
  const requestPath = "/parks";
  const url = new URL(`${apiHost}${requestPath}/${id}`);

  const requestInit: RequestInit = {
    method: "DELETE",
  };

  return fetch(url, requestInit);
}

export default deleteParkSlot;
