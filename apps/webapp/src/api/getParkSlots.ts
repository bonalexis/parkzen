const apiHost = process.env.REACT_APP_API_HOST;

async function getParkSlots() {
  const requestPath = "/parks";
  const url = new URL(`${apiHost}${requestPath}`);

  return fetch(url).then((res) => res.json());
}

export default getParkSlots;
