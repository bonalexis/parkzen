import { useEffect, useState } from "react";
import FormCreateParkSlot from "../components/ParkSlot/FormCreateParkSlot";
import ListParkSlot from "../components/ParkSlot/ListParkSlot";
import Main from "../layouts/Main";
import getParkSlots from "../api/getParkSlots";

function Parking() {
  const [parkSlotList, setParkSlotList] = useState<ParkSlots[]>([]);

  useEffect(() => {
    getParkSlots().then((parkSlots) => setParkSlotList(parkSlots));
  }, []);

  return (
    <Main>
      <h1>Parking</h1>
      <h2 className="mt-5">Formulaire de création d'une place de parking</h2>
      <FormCreateParkSlot />

      <h2 className="mt-5">Liste de toutes les places de parking</h2>
      <ListParkSlot parkSlotList={parkSlotList} />
    </Main>
  );
}

export default Parking;
