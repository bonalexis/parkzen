import { useEffect, useState } from "react";
import ListParkSlot from "../components/ParkSlot/ListParkSlot";
import Main from "../layouts/Main";
import getParkSlots from "../api/getParkSlots";
import { Button, Form } from "react-bootstrap";
import getParkSlotById from "../api/getParkSlotById";

function ParkSlotUser() {
  const [parkSlotList, setParkSlotList] = useState<ParkSlots[]>([]);
  const [renterId, setRenterId] = useState("");

  const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    getParkSlotById(renterId).then((parkSlots) => setParkSlotList(parkSlots));
  };

  useEffect(() => {
    getParkSlots().then((parkSlots) => setParkSlotList(parkSlots));
  }, []);

  return (
    <Main>
      <h1>Places d'un propriétaire</h1>
      <h2 className="mt-5">Rechercher les places d'un propriétaire</h2>
      <Form onSubmit={submitForm}>
        <Form.Group className="mb-3" controlId="FirstName">
          <Form.Label>Identifiant</Form.Label>
          <Form.Control
            type="text"
            placeholder="L'identifiant"
            value={renterId}
            onInput={(event) => setRenterId(event.currentTarget.value)}
          />
        </Form.Group>
        <Button type="submit">Rechercher</Button>
      </Form>

      <h2 className="mt-5">Liste des places</h2>
      <ListParkSlot parkSlotList={parkSlotList} />
    </Main>
  );
}

export default ParkSlotUser;
