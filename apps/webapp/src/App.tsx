import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./routes/Home";
import Parking from "./routes/Parking";
import ParkSlotUser from "./routes/ParkSLotsUser";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/parking",
    element: <Parking />,
  },
  {
    path: "/find-parks",
    element: <ParkSlotUser />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
