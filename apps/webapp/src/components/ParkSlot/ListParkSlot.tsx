import { Button, Placeholder, Table } from "react-bootstrap";
import imgDeleteBin from "../../assets/images/delete-bin.svg";
import deleteParkSlot from "../../api/deleteParkSlot";

type ListParkSlotProps = {
  parkSlotList: ParkSlots[];
};

function ListParkSlot({ parkSlotList }: ListParkSlotProps) {
  return (
    <Table striped bordered>
      <thead>
        <tr>
          <th>Prénom</th>
          <th>Nom</th>
          <th>Prix pour une heure</th>
          <th>Identifiant Propriétaire</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        {parkSlotList.length ? (
          parkSlotList.map((parkSlot, id) => (
            <tr key={`parkSlot-${id}`}>
              <td>{parkSlot.firstName}</td>
              <td>{parkSlot.lastName}</td>
              <td>{parkSlot.priceForOneHour}</td>
              <td>{parkSlot.renterId}</td>
              <td>
                <Button
                  variant="danger"
                  onClick={() => deleteParkSlot(parkSlot.id)}
                >
                  <img
                    src={imgDeleteBin}
                    alt="supprimer"
                    style={{ width: "1em" }}
                  />
                </Button>
              </td>
            </tr>
          ))
        ) : (
          <tr>
            <td>
              <Placeholder animation="glow">
                <Placeholder xs={6} />
              </Placeholder>
            </td>
            <td>
              <Placeholder animation="glow">
                <Placeholder xs={6} />
              </Placeholder>
            </td>
            <td>
              <Placeholder animation="glow">
                <Placeholder xs={6} />
              </Placeholder>
            </td>
            <td>
              <Placeholder animation="glow">
                <Placeholder xs={6} />
              </Placeholder>
            </td>
            <td>
              <Placeholder animation="glow">
                <Placeholder xs={6} />
              </Placeholder>
            </td>
          </tr>
        )}
      </tbody>
    </Table>
  );
}

export default ListParkSlot;
